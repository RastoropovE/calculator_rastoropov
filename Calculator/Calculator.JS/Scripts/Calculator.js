﻿var Calculator = (function () {

    var memoryValue = 0; //  memory cell for temporaly value
    var BufValue = 0; //curren
    var curExpression=""; // string repr of expression
    var CurDisplay = 0;

    // commands
    var add = function (val1, val2) {
        if (val1 == undefined && val2 == undefined) return "+";
        return parseFloat(val1) + parseFloat(val2);
    };
    var sub = function (val1, val2) {
        if (val1 == undefined && val2 == undefined) return  "-";
        return parseFloat(val1) - parseFloat(val2);
    };
    var mul = function (val1, val2) {
        if (val1 == undefined && val2 == undefined) return "*";
        return parseFloat(val1) * parseFloat(val2);
    };
    var div = function (val1, val2) {
        if (val1 == undefined && val2 == undefined) return "/";
        return parseFloat(val1) / parseFloat(val2);
    };
    var pow = function (val1, val2) {
        if (val1 == undefined && val2 == undefined) return "^";
        return Math.pow(parseFloat(val1), parseFloat(val2));
    };
    var empty = function (val1, val2) { return val2; };
    // -end commands

    var currentCommand = empty;

    // special
    var sqrt = function (val1) {
        CurDisplay = Math.sqrt(parseFloat(val1));
        return CurDisplay;
    };
    var pers = function (val1) {
        CurDisplay = parseFloat(val1)/100;
        return CurDisplay;
    };
    var onedivonx = function (val1) {
        CurDisplay =1/ parseFloat(val1);
        return CurDisplay;
    };
    var memPlus = function (val1) {
        CurDisplay = parseFloat(val1);
        memoryValue += CurDisplay;
        return CurDisplay;
    };
    var memMinus = function (val1) {
        CurDisplay = parseFloat(val1);
        memoryValue -= CurDisplay;
        return CurDisplay;
    };
    var memSet = function (val1) {
        CurDisplay = parseFloat(val1);
        memoryValue = CurDisplay;
        return CurDisplay;
    };
    var memRetr = function (val1) {
        CurDisplay = memoryValue;
        return CurDisplay;
    };
    var memClr = function (val1) {
        memoryValue = 0;
        return CurDisplay;
    };
    var changeSign = function (val1) {
        CurDisplay = -parseFloat(val1);
        return CurDisplay;
    };
    var backspace = function (val1) {
        var tmp = val1.substring(0, val1.length - 1);
        CurDisplay = tmp.length!=0?parseFloat(tmp):0;
        return CurDisplay;
    };
    var clear = function (val1) {
        CurDisplay = 0;
        return CurDisplay;
    };
    var totalclear = function (val1) {
        curExpression = "";
        buf = 0;
        CurDisplay = 0;
        return CurDisplay;
    };
    var sin = function (val1) {
        CurDisplay = Math.sin(parseFloat(val1));
        return CurDisplay;
    };
    var cos = function (val1) {
        CurDisplay = Math.cos(parseFloat(val1));
        return CurDisplay;
    };
    var tan = function (val1) {
        CurDisplay = Math.tan(parseFloat(val1));
        return CurDisplay;
    };



    // - end special


    return {
        getCurrentExpr: function () { return curExpression; },
        getMemoryValue: function(){return memoryValue;},
        test: function () { console.log("i work"); },

        setValue: function (val1) {
            if (val1.length == 0) val1 = "0";
            CurDisplay = parseFloat(val1);
        },

        setCommand: function (cmd) {
            BufValue = currentCommand(BufValue, CurDisplay);
            curExpression += CurDisplay + cmd();
            CurDisplay = 0;
            currentCommand = cmd;
        },

        executeAll: function () {
            CurDisplay = currentCommand(BufValue, CurDisplay);
            currentCommand = empty;
            curExpression = "";
            BufValue = 0;
            return CurDisplay;
        },

        singleActions: {
            '√': sqrt,
            '%': pers,
            'M+': memPlus,
            'M-': memMinus,
            'MS': memSet,
            'MR': memRetr,
            'MC': memClr,
            '1/x': onedivonx,
            '+/-': changeSign,
            '←': backspace,
            'C': clear,
            'CE': totalclear,

            'sin': sin,
            'cos': cos,
            'tan': tan,
        },

        commands: {
            '+': add,
            '-': sub,
            '*': mul,
            '/': div,
            '^': pow,
        },
    }

}());