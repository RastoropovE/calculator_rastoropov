﻿

    var commands = document.getElementsByClassName("command");
    var actions = document.getElementsByClassName("action");
    var disp = document.getElementById("display");
    var expr = document.getElementById("expr");
    var exec = document.getElementById("exec");
    var mem = document.getElementById("memory");
    disp.focus();

    //set event listeners 
    for (var i = 0; i < actions.length; i++) { actions[i].addEventListener("click", execAction); }
    for (var i = 0; i < commands.length; i++) { commands[i].addEventListener("click", commandClick); }
    exec.addEventListener("click", function () {
        Calculator.setValue(disp.value);
        disp.value = Calculator.executeAll().toString();
        expr.innerText = Calculator.getCurrentExpr();
        disp.focus();
    });
    // event handlers
    function commandClick() {
        Calculator.setValue(disp.value);
        disp.value = "0";
        Calculator.setCommand(Calculator.commands[this.value]);
        expr.innerText = Calculator.getCurrentExpr();
        disp.focus();
    }
    function execAction() {
        //console.log(this.value);
        disp.value = Calculator.singleActions[this.value](disp.value);
        expr.innerText = Calculator.getCurrentExpr();
        disp.focus();
        mem.innerText = Calculator.getMemoryValue();
    }

    disp.oninput = function () {
        if (disp.value[0] == "0")
            disp.value = disp.value.substr(1, 1);
        if (disp.value == "")
            disp.value = "0";
    }
