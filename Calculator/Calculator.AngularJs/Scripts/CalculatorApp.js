﻿var app = angular.module('CalculatorApp', []);

app.controller('Main', function ($scope) {
    $scope.numberRows = [
            ['0', '1', '2', '3', '4', ],
            ['5', '6', '7', '8', '9', '.'],
    ];
    $scope.binaryCommandsList = ['+', '-', '/', '*', '^', ];

    $scope.actionsList = [ ['√', '%', '1/x', '+/-', 'GT', '='],
    ['CE', 'C', '←'],   ['M+', 'M-', 'MR', 'MC', ], ];


    $scope.MemoryValue = 0;
    $scope.BufDisplay = "0";
    $scope.CurDisplay = "0";

    var previousResults = []; //  for GT(grand total action)


    // Bin commands
    var add = function (val1, val2) { return parseFloat(val1) + parseFloat(val2); }
    var sub = function (val1, val2) { return parseFloat(val1) - parseFloat(val2); }
    var mul = function (val1, val2) { return parseFloat(val1) * parseFloat(val2); }
    var div = function (val1, val2) { return parseFloat(val1) / parseFloat(val2); }
    var pow = function (val1, val2) { return Math.pow(parseFloat(val1) , parseFloat(val2)) }
    // Special actions
    var sqr = function () { $scope.CurDisplay = Math.sqrt(parseFloat($scope.CurDisplay)).toString(); }
    var prs = function () { $scope.CurDisplay= parseFloat($scope.CurDisplay) / 100; }
    var onedivx = function () { $scope.CurDisplay = 1 / parseFloat($scope.CurDisplay); }
    var invert = function () { $scope.CurDisplay = -parseFloat($scope.CurDisplay);  }
    var clear = function () { $scope.CurDisplay = "0"; }
    var clearTotal = function () { $scope.CurDisplay = "0"; $scope.BufDisplay = "0"; $scope.MemoryValue="0" }

    var equ = function () {
        if (isResult) return;
        $scope.CurDisplay = curCommand($scope.BufDisplay, $scope.CurDisplay);
        $scope.BufDisplay = "0";
        curCommand = empty;
        previousResults.push(parseFloat($scope.CurDisplay));
        isResult = true;
    }

    var memplus = function () { $scope.MemoryValue += parseFloat($scope.CurDisplay); }
    var memminus = function () { $scope.MemoryValue -= parseFloat($scope.CurDisplay); }
    var memclear = function () { $scope.MemoryValue = 0; }
    var memget = function () { $scope.CurDisplay = $scope.MemoryValue; }

    var empty = function (val1, val2) { return val2; };
    var grandtotal = function() {
        var c = 0;
        for (var i = 0; i < previousResults.length; i++) c += previousResults[i];
        $scope.CurDisplay = c;
    };
    var bcsp = function () {
        $scope.CurDisplay = $scope.CurDisplay.toString();
        $scope.CurDisplay = $scope.CurDisplay.substr(0, $scope.CurDisplay.length - 1);
        if ($scope.CurDisplay.length == 0) $scope.CurDisplay = "0";
    };
    /* -end commands*/

    var isResult = false;
    var curCommand = empty;
    
    $scope.AddNumber = function(ch) {
            if ($scope.CurDisplay == "0" || isResult) {
                $scope.CurDisplay = "";
                isResult = false;
            }
            $scope.CurDisplay += ch;
    };

    $scope.setNewCommand = function (cmd) {
        console.log(cmd);
        $scope.BufDisplay = curCommand($scope.BufDisplay, $scope.CurDisplay);
        $scope.CurDisplay = "0";
        curCommand = $scope.binaryCommands[cmd];
    }


    $scope.binaryCommands = {
        '+': add,
        '-': sub,
        '/': div,
        '*': mul,
        '^': pow,
    };

    $scope.actions = {
        '√' : sqr,
        '←': bcsp,
        "1/x": onedivx,
        "+/-": invert,
        "%": prs,
        "M+": memplus,
        "M-": memminus,
        "MR": memget,
        "MC": memclear,
        "GT": grandtotal,
        "=": equ,
        'C': clear,
        'CE': clearTotal,
    };
    
});
